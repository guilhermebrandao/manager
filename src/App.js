import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';

import Router from './Router';

class App extends Component {
  componentWillMount() {
    const config = {
      apiKey: 'AIzaSyCuVqYnYOflj1jdQmSVrUx7FtRrj4zcW0A',
      authDomain: 'manager-44303.firebaseapp.com',
      databaseURL: 'https://manager-44303.firebaseio.com',
      projectId: 'manager-44303',
      storageBucket: 'manager-44303.appspot.com',
      messagingSenderId: '400152967062'
    };
    firebase.initializeApp(config);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
